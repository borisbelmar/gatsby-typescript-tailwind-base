module.exports = {
  parser: '@typescript-eslint/parser', // Specifies the ESLint parser
  extends: [
    'airbnb-typescript',
    'airbnb/hooks',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  settings: {
    react: {
      version: 'detect'
    }
  },
  env: {
    browser: true,
    node: true,
    es6: true
  },
  plugins: ['@typescript-eslint', 'react'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
    project: './tsconfig.json'
  },
  rules: {
    'react/prop-types': 'off', // Disable prop-types as we use TypeScript for type checking
    'react/no-unescaped-entities': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    'linebreak-style': 'off',
    semi: 'off',
    '@typescript-eslint/semi': [2, 'never'],
    'import/no-extraneous-dependencies': 0,
    'no-console': 0,
    '@typescript-eslint/explicit-module-boundary-types': 0,
    'comma-dangle': 'off',
    '@typescript-eslint/comma-dangle': [2, 'never'],
    'object-curly-newline': 0,
    'react/jsx-one-expression-per-line': 0,
    'arrow-body-style': 0,
    'consistent-return': 0,
    'jsx-a11y/label-has-associated-control': 0,
    'react/button-has-type': 0,
    'arrow-parens': [2, 'as-needed'],
    'class-methods-use-this': 0,
    '@typescript-eslint/no-unused-vars': [1, { argsIgnorePattern: '^_' }],
    'react/jsx-closing-bracket-location': 0
  },
  overrides: [
    // Override some TypeScript rules just for .js files
    {
      files: ['*.js'],
      rules: {
        '@typescript-eslint/no-var-requires': 'off' //
      }
    }
  ]
}
