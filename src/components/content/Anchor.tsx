import React from 'react'
import { Link } from 'gatsby'
import clsx from 'clsx'

interface AnchorProps {
  to: string
  className?: string
  targetBlank?: boolean
}

const Anchor: React.FC<AnchorProps> = ({ to, children, targetBlank, className }) => {
  const anchorStyle = clsx('anchor', className)
  if (to.match(/^(http|https):\/\//)) {
    return targetBlank ? (
      <a className={anchorStyle} href={to} target="_blank" rel="noreferrer noopener">{children}</a>
    ) : (
      <a className={anchorStyle} href={to}>{children}</a>
    )
  }
  return <Link className={anchorStyle} to={to}>{children}</Link>
}

export default Anchor
