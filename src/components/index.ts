export { default as DefaultLayout } from './layout/DefaultLayout'

export { default as Image } from './content/Image'
export { default as Anchor } from './content/Anchor'

export { default as SEO } from './core/SEO'
