import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Header from './components/Header'

import '../../assets/global.css'

const Layout: React.FC = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <>
      <Header siteTitle={data.site.siteMetadata?.title || 'Title'} />
      <div className="my-0 mx-auto py-6 px-8 md:w-9/12">
        <main>{children}</main>
      </div>
      <footer className="py-5 my-0 mx-auto px-8 md:w-9/12">
        © {new Date().getFullYear()}, Built with{' '}
        <a href="https://www.gatsbyjs.com">Gatsby</a>
      </footer>
    </>
  )
}

export default Layout
