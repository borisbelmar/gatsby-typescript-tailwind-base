import { Link } from 'gatsby'
import React from 'react'

interface HeaderProps {
  siteTitle: string
}

const Header: React.FC<HeaderProps> = ({ siteTitle }) => (
  <header className="bg-primary-500">
    <div className="my-0 mx-auto py-6 px-8 md:w-9/12">
      <h1 className="text-2xl font-bold">
        <Link to="/" className="text-white">
          {siteTitle}
        </Link>
      </h1>
    </div>
  </header>
)

export default Header
