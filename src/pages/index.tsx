import React from 'react'

import { DefaultLayout, SEO, Image, Anchor } from '../components'

const IndexPage = () => (
  <DefaultLayout>
    <SEO title="Home" />
    <h1 className="text-2xl mb-5 font-bold">Hi people</h1>
    <p className="mb-2">Welcome to your new Gatsby site.</p>
    <p className="text-black text-opacity-50">Now go build something great.</p>
    <div className="mb-5 w-96">
      <Image />
    </div>
    <Anchor to="/page-2/">Go to page 2</Anchor>
  </DefaultLayout>
)

export default IndexPage
