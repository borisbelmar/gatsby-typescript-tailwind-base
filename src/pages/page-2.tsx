import React from 'react'

import { Anchor, DefaultLayout, SEO } from '../components'

const SecondPage = () => (
  <>
    <SEO title="Page two" />
    <DefaultLayout>
      <h1 className="text-2xl font-bold">Hi from the second page</h1>
      <p className="mb-5">Welcome to page 2</p>
      <Anchor to="/">Go back to the homepage</Anchor>
    </DefaultLayout>
  </>
)

export default SecondPage
