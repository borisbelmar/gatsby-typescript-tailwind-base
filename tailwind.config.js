/* eslint-disable global-require */
module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true
  },
  purge: {
    content: ['./src/**/*.tsx'],
    options: {
      whitelist: ['mode-dark']
    }
  },
  theme: {
    aspectRatio: {
      none: 0,
      square: 1,
      '16/9': [16, 9],
      '4/3': [4, 3]
    },
    extend: {
      colors: {
        primary: {
          100: '#e8dbef',
          200: '#d0b7df',
          300: '#b993cf',
          400: '#a16fbf',
          500: '#8a4baf',
          600: '#6e3c8c',
          700: '#532d69',
          800: '#371e46',
          900: '#1c0f23'
        }
      },
      spacing: {
        72: '18rem',
        84: '21rem',
        96: '24rem'
      }
    }
  },
  variants: {},
  plugins: [
    require('@tailwindcss/custom-forms'),
    require('tailwindcss-aspect-ratio'),
    require('tailwindcss-dark-mode')()
  ]
}
